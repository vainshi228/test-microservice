#!/bin/bash
### folder code-gen
for FOLDER in 'add' 'customer' 'order' 'payment' 'seller'
do
  >./$FOLDER/config/default.json cat <<-EOF
{
  "postgres": {

  },
  "amqt": {

  }
}
EOF
  >./$FOLDER/config/production.json cat <<-EOF
{
  "postgres": {

  },
  "amqt": {

  }
}
EOF
  >./$FOLDER/config/development.json cat <<-EOF
{
  "postgres": {

  },
  "amqt": {

  }
}
EOF

done